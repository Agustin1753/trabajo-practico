using UnityEngine;
public class ControlBot : MonoBehaviour 
{ 
    private int hp; 
    void Start() 
    { 
        hp = 100; 
    } 
    public void recibirDaņo() 
    {
        hp = hp - 25; 
        if (hp <= 0) 
        {
            this.desaparecer();
        }
    } 
    private void desaparecer()
    { 
        Destroy(gameObject); 
    } 
}