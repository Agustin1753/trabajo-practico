using UnityEngine;
public class plataforma : MonoBehaviour
{
    public float rapidez = .5f;
    public float strength = 9f;
    private float randomOffset;

    void Update()
    {
        Vector3 pos = transform.position;
        pos.x = Mathf.Sin(Time.time * rapidez + randomOffset) * strength;
        transform.position = pos;
    }
}