using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
public class ControlJugador : MonoBehaviour
{
    public GameObject gameover;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public float rapidez;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public GameObject proyectil;
    public Camera camaraPrimeraPersona;
    public float rapidezDesplazamiento = 10.0f;
    public bool estaAgachado = false;
    public float alturaAgachado = 0.5f;
    public float alturaParado = 1.0f;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 5);
        }
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                estaAgachado = true;
                transform.localScale = new Vector3(1, alturaAgachado, 1);
            }
            if (Input.GetKeyUp(KeyCode.C))
            {
                estaAgachado = false;
                transform.localScale = new Vector3(1, alturaParado, 1);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            float x = 0.05f;
            float y = 0.05f;
            float z = 0.05f;
            float aceleracion = 0.02f;
            transform.localScale += new Vector3(x, y, z);
            rapidez += aceleracion;
            magnitudSalto = magnitudSalto + 1;
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Vacio") == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 6)
        {
            textoGanaste.text = "Ganaste!";
        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    void FixedUpdate()
    {
        if (estaAgachado)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, alturaAgachado))
            {
                transform.position = hit.point;
            }
        }
    }
}